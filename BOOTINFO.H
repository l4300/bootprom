#ifndef	_BOOTINFO_H
#define	_BOOTINFO_H

enum BootMode {MODE_HEADER, MODE_CODE, MODE_DATA, MODE_COPY, MODE_FAILED, MODE_DONE};
#define	BFLAGS_FORFLASH		0x01	/* downloading for flash programming */
#define	BFLAGS_DECOMPRESS	0x02	/* need to decompress this section */
#define	BFLAGS_RUNABLE		0x04	/* ready to run */
#define	BFLAGS_CODELOCATED	0x08	/* code already located and programmed */

#define	IFLAGS_CODECOMPRESSED	0x01
#define	IFLAGS_DATACOMPRESSED	0x02
#define	IFLAGS_FORFLASH		0x04

typedef struct	_bootinfo {
	unsigned	b_rcount;		// count of relocation items
	unsigned	b_codesize;		// uncompressed code size in para
	unsigned	b_ccodepara;		// paragraphs of compressed code size
	unsigned	b_ccodebyte;		// bytes left over in last paragraph
	long		b_codebytes;
	unsigned	b_datasize;		// size of uncompressed data segment, bytes
	unsigned	b_cdatasize;		// size of compressed data segment
	unsigned	b_stackseg;
	unsigned	b_stackptr;
	unsigned	b_minfreep;
	unsigned	b_iip;
	unsigned	b_ics;
	unsigned	b_flags;
	unsigned	b_dateh;
	unsigned	b_datel;
	unsigned	b_ccsum;
	unsigned	b_dcsum;
	unsigned	b_abscsp;		// absolute CS for bios
	unsigned	b_absdsp;		// absolute DS for bios
	unsigned 	b_class;		// which class this program is

	unsigned char huge *b_data;		// where to copy data
	long		b_loadbytes;		// how many bytes to copy
	long		b_outbytes;		// how many bytes remaining for decompress
	long		b_offset;		// offset in file of copy block base

	enum BootMode 	b_mode;			// current boot mode
	enum BootMode	b_nextmode;		// next mode to move to
	unsigned	b_modeflags;		// for flash, or decompress
	char		b_ident[80];
	SCD	far	*b_scd;			// where our scd is
	unsigned int far * far *b_reloc;        // where to put reloc items
	unsigned char far *b_dataloc;		// where to put data segment
	unsigned char far *b_codeloc;		// where to put code segment

	unsigned int	b_dataseg;		// used by relocator
	unsigned int	b_codeseg;		// used by relocator
	unsigned int	b_xstackseg;		// relocated stack segment
	unsigned int	b_xics;			// initial code segment
	unsigned int	b_xiip;			// initial code ip
	unsigned int	b_sp;			// initial stack ptr

	unsigned int	b_item;			// reloc item offset
	unsigned int	b_csum;			// running csum for located code
	unsigned int	b_csector;			// code sector
} Boot_Info;


#endif
