/*------------------------------------------------------------------------
 * filename - ctime.c
 *
 * function(s)
 *    comtime   - converts long timedate to a structure
 *    atime     - converts date and time to ASCII without trailing '\n'
 *    asctime   - converts date and time to ASCII
 *    ctime     - converts date and time to a string
 *    gmtime    - converts date and time to Greenwich Mean Time
 *    localtime - converts date and time to a structure
 *    mktime    - normalizes the date and time structure
 *    strftime  - converts date and time structure to a string
 *-----------------------------------------------------------------------*/

/*
 *      C/C++ Run Time Library - Version 5.0
 *
 *      Copyright (c) 1987, 1992 by Borland International
 *      All Rights Reserved.
 *
 */


#include <time.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

static const char Days[12] = {
  31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31
  };

static int YDays[12] = {
  0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334
  };

static char *SWeekday[7] = {
  "Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"
  };

static char *LWeekday[7] = {
  "Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"
  };

static char *SMonth[12] = {
  "Jan", "Feb", "Mar", "Apr", "May", "Jun",
  "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
  };

static char *LMonth[12] = {
  "January", "February", "March",     "April",   "May",      "June",
  "July",    "August",   "September", "October", "November", "December"
  };

static char *AmPm[2] = {
  "AM", "PM"
  };

static  struct  tm tmX;

/*---------------------------------------------------------------------*

Name            comtime

Usage           static struct tm *comtime(unsigned long time, int dst);

Prototype in    local to this module

Description     fills the time structure tm by translating the long
		time.

Return value    the broken down time structure. This structure is
		a static which is overwritten with each call.

*---------------------------------------------------------------------*/
static  struct  tm *comtime(time_t time, int dst)
{
    int      hpery;
    unsigned i;
    unsigned cumdays;

    if (time < 0)
	time = 0;

    (tmX).tm_sec = (int)(time % 60);
    time /= 60;                             /* Time in minutes */
    (tmX).tm_min = (int)(time % 60);
    time /= 60;                             /* Time in hours */
    i = (unsigned)(time / (1461L * 24L));   /* Number of 4 year blocks */
    (tmX).tm_year = (i << 2);
    (tmX).tm_year+= 70;
    cumdays = 1461 * i;
    time %= 1461L * 24L;        /* Hours since end of last 4 year block */

    for (;;)
	{
        hpery = 365 * 24;
	if (((tmX).tm_year & 3) == 0)
            hpery += 24;
        if (time < hpery)
	    break;
	cumdays += hpery / 24;
	(tmX).tm_year++;
	time -= hpery;
	}   /* at end, time is number of hours into current year */

	(tmX).tm_isdst = 0;

    (tmX).tm_hour = (int)(time % 24);
    time /= 24;             /* Time in days */
    (tmX).tm_yday = (int)time;
    cumdays += (int)time + 4;
    (tmX).tm_wday = cumdays % 7;
    time++;

    if (((tmX).tm_year & 3) == 0)
        {
        if (time > 60)
            time--;
	else
            if (time == 60)
                {
		(tmX).tm_mon = 1;
		(tmX).tm_mday = 29;
		return(&(tmX));
                }
        }

    for ((tmX).tm_mon = 0; Days[(tmX).tm_mon] < time; (tmX).tm_mon++)
	time -= Days[(tmX).tm_mon];

    (tmX).tm_mday = (int)(time);
    return(&(tmX));
}


/*------------------------------------------------------------------------*

Name            atime - converts date and time to ASCII without trailing '\n'

Usage           #include <time.h>
                static int atime( char *dest, const struct tm *tmPtr );

                Local to this module

Description     Provides the basic formatting capabilities for asctime() and
                strftime( "%c" ).  These two functions both provide an ASCII
                version of the date and time in the struct tm, but asctime()
                adds a newline to the end.

Return value    Returns the number of characters in the ASCII string

*---------------------------------------------------------------------------*/

static int atime( char *dest, const struct tm *tmPtr )
{
	char	*zero = "0";

    return sprintf( dest, "%s %s %s%d %s%d:%s%d:%s%d %4d",
	SWeekday[tmPtr->tm_wday],
	SMonth[tmPtr->tm_mon],
	tmPtr->tm_mday < 10 ? zero : "", tmPtr->tm_mday,
	tmPtr->tm_hour < 10 ? zero : "",tmPtr->tm_hour,
	tmPtr->tm_min < 10 ? zero : "",tmPtr->tm_min,
	tmPtr->tm_sec < 10 ? zero : "",tmPtr->tm_sec,
        tmPtr->tm_year + 1900
        );
}

/*------------------------------------------------------------------------*

Name            asctime   - converts date and time to ASCII
                ctime     - converts date and time to a string
                gmtime    - converts date and time to Greenwich Mean Time
                localtime - converts date and time to a structure

Usage           #include <time.h>
		char *asctime(struct tm *_QRTLInstanceData(tmX));
                char *ctime(long *clock);
                struct tm *gmtime(long *clock);
                struct tm *localtime(long *clock);

Prototype in    time.h

Description     asctime  converts  a  time  stored  as  a  structure  to  a
                26-character string in the following form:

                Mon Nov 21 11:31:54 1983\n\0

                All the fields have a constant width.

                ctime converts a time pointed to by clock (such as returned
                by the function time) to  a 26-character string of the form
                described above.

                localtime   and  gmtime   return  pointers   to  structures
		containing the broken-down time. localtime corrects for the
		time  zone  and  possible  daylight  savings  time;  gmtime
                converts directly to GMT.

                The global  long variable timezone contains  the difference
		in  seconds between  GMT and  local standard  time (in EST,
                timezone  is  5*60*60).  The  global  variable  daylight is
                non-zero  if  and  only  if  the  standard  U.S.A. Daylight
		Savings Time conversion should be applied.

Return value    asctime and ctime return a  pointer to the character string
                containing the date and time. This string is a static which
                is overwritten with each call.

                gmtime and localtime return the broken down time structure.
                This structure is  a static which is overwritten  with each
                call.

*---------------------------------------------------------------------------*/
struct  tm      *Xgmtime(const time_t *clock)
{
    return(comtime(*clock, 0));
}

char    *Xasctime(const struct tm *tmPtr)
{
    static char a[26];

    int end;

    end = atime( (a), tmPtr );
    strcat(a,"\n");

    return((a));
}

char    *Xctime(const time_t *clock)
{
    return(Xasctime(Xgmtime(clock)));
}

