/* File: splay.h
   Copyright (C) 1990 Brad K. Clements

   Contains data structure definitions for splay-tree data compression,
   based on
	"Applications of splay trees to data compression",
	Douglas W. Jones,
	Communications of the ACM
	August 1988, Volume 31, Number 8
*/

#define	MAX_SPLAY_STATES	48	/* max number of splay states
					   bytes used is approximately
					    1537 bytes per state, 16 states is
					   24592 bytes, quite a lot */
#define	MAX_SPLAY_ENTRIES	16	/* total number of streams that
					   can be compressed */


#define MAXCHAR		255	/* maximum source character code */
#define SUCCMAX		(MAXCHAR+1)
#define TWICEMAX 	((2*MAXCHAR)+1)
#define SUCCTWICE 	(TWICEMAX+1)


typedef	struct  splay_state_data {
	unsigned short 	ssd_left[SUCCMAX];
	unsigned short		ssd_right[SUCCMAX];
	unsigned char		ssd_up[SUCCTWICE];
	unsigned char		ssd_dirty_flag;
} SSD;

typedef	struct	splay_control_data {
	unsigned int		scd_a;
	unsigned char		scd_states;		/* number of states */
	unsigned char		scd_curstate;
	unsigned char		scd_bitbuffer;
	int			scd_bitcount;
	SSD			scd_ssd[MAX_SPLAY_STATES];
} SCD;

#define	SPLAY(scd, cch) 	\
	/* SCD *scd;	*/\
	/* unsigned char cch; */	\
{ 							\
	/* scd is SCD, ch is unsigned char to splay */ 				\
	register  SSD	*ssd = &((SCD *)(scd))->scd_ssd[((SCD *)(scd))->scd_curstate];	\
	register  unsigned short a,b; 							\
	register  unsigned char  c,d;  						\
	a = cch + SUCCMAX;							\
	do {									\
		if( ((c = ssd->ssd_up[a]) & ~1)) {	/* a pair remains ... eck */ 	\
			if( c == (b = ssd->ssd_left[d = ssd->ssd_up[c]]))  {		\
				b = ssd->ssd_right[d];				\
				ssd->ssd_right[d] = a;				\
			}							\
			else 							\
				ssd->ssd_left[d] = a;				\
			if( ssd->ssd_left[c] == a)				\
				ssd->ssd_left[c] = b;				\
			else							\
				ssd->ssd_right[c] = b;				\
			a = (ssd->ssd_up[a] = d); /* hope your compiler does this ok */ 	\
			ssd->ssd_up[b] = c;					\
		}								\
		else								\
			a = c;							\
	} while (a & ~1);							\
	((SCD *)scd)->scd_curstate = cch % ((SCD *)scd)->scd_states;		\
}

#ifdef	__cplusplus
extern "C" {
#endif

void 	splay_init(SCD *scd);
void	splay_tree_init(SSD *ssd);
int	splay_uncompress(SCD *scd, unsigned char far *ibuff, unsigned int ilen, unsigned char far *obuff, unsigned int olen,int flushem);
unsigned int	splay_compress(SCD *scd, unsigned char far * chin, unsigned int count,unsigned char far * obuff,unsigned int olen, int *flushbits);

#ifdef	__cplusplus
}
#endif
