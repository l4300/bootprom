/* flash.h - flash file system defs,
   Copyright (C) 1993 MurkWorks Inc, All Rights Reserved
*/
#ifndef	_FLASH_H
#define	_FLASH_H

#define	FFILENAME_SIZE	16
#define	FRESERVED_SIZE	22

typedef struct _file_header {
	char		f_name[FFILENAME_SIZE];
	char		f_dummy;	/* mandatory null byte */
	long		f_filesize;	/* does include header */
	unsigned char	f_type;		/* file types */
#define	FTYPE_TEXT	0x01		/* text file, like control file */
#define	FTYPE_LCODE	0x02		/* is a located executable code segment */
#define	FTYPE_LDATA	0x03		/* located data for previous code */
#define	FTYPE_RCODE	0x04		/* relocatable code and data object */
#define	FTYPE_ACODE	0x05		/* absolute code/data object. */
#define	FTYPE_BOOT	0x06		/* boot block */
#define	FTYPE_EXEC	0x07		/* executable - located code, relocatable data
					   with bootblock in last paragraph of
					   directory entry */
#define	FTYPE_FREE	0x80		/* free space */

	long		f_ufilesize;	/* uncompressed or natural file size w/o header */
	int		f_fcsum;	/* check sum over file */
	unsigned char	f_mode;		/* mode flags */
#define	FMODE_COMPRESSED		0x0001	/* splay compressed */
#define	FMODE_READONLY			0x0002	/* can't be erased */
#define	FMODE_RELOCATE			0x0004 	/* file needs copying to ... */
	unsigned long	f_date;		/* standard time_t */
	unsigned long	f_data;		/* variable data,
					   has relocation segment for relocatable
					   or abs objects */
	unsigned 	f_sector;	/* first `sector' of file */
	unsigned char	f_class;	/* file type class */
#define	FCLASS_NONE		0
#define	FCLASS_PRIMARY		1
#define	FCLASS_SECONDARY	2
#define	FCLASS_DIAGNOSTIC	255
	unsigned int	f_dcsum;	/* directory entry checksum */
	unsigned char 	f_reserved[FRESERVED_SIZE];
} FileHeader;

#define	FOFFSET(fh)	FlashFileOffset(fh)
#define	FHSIZE(fh)	(fh)->f_filesize

#if	sizeof(FileHeader) != 64
#error FileHeader size is off
#endif

typedef struct _bootblock {
	unsigned	b_cs;			/* start code segment */
	unsigned	b_ip;			/* start ip */
	unsigned	b_ss;			/* already relocated */
	unsigned	b_sp;
	unsigned	b_ds;			/* where to expand data to */
	unsigned	b_udatasize;		/* uncompressed data size */
	unsigned	b_codesize;		/* code seg size in para */
} BootBlock;
#if	sizeof(BootBlock) > FRESERVED_SIZE
#error BootBlock is too big or f_reserved is too small
#endif

#define	MAX_DIRECTORY_ENTRIES	16 	/* number of directory entries */
#define	SECTOR_SIZE		128	/* size of a sector */

/* this control block is located in the very first segment */
#define	BFILENAME_LENGTH	64
typedef struct _control_block {
	unsigned int 	cb_tag;		/* ident as control block */
#define	F_IDCTAG	0xa5a5
	unsigned char	cb_version;	/* version */
#define	CBVERSION	0x01
	unsigned char	cb_pad;
	int		cb_csum;	/* check sum over control block */
	unsigned int	cb_directory_offset;	/* offset of dir in paras from cb */
	unsigned int	cb_directory_size;	/* size in entries */
	unsigned int	cb_writes;	/* count of writes to the cb block */
	IPaddr		cb_serverip;	/* where to get new boot load */
	char		cb_bootfile[BFILENAME_LENGTH];
	BootBlock	cb_bootblock;
} ControlBlock;


FileHeader huge *FlashAllocSpace(long size, int firstlast, FileHeader far *h);	/* alloc a block this big */
					/* set first true for low mem alloc
					   you must provide space for a fileheader
					   alloc'd in h which will be used to copy
					   into a properly formatted header
					*/
ControlBlock far *FlashCB();
int	FlashSetupProgram(Boot_Info *, int fclass, char *name);

// external defs
int	FlashCFileSize(SCD far *scd, unsigned char far *data, int ilen, unsigned char far *output, int olen);
FileHeader far * FlashFindFile(char far *filename);
FileHeader far *FlashFindFileClass(unsigned char fclass);	// get dir of this class
void	FlashFileList(int type);	// list files
int	FlashReadFile(SCD far *scd, FileHeader far *fh, unsigned char far *output, int olen);
int	FlashWriteFile(char far *filename, unsigned char far *input, int len, int ucsize, int mode, long date);
int	FlashFreeFile(FileHeader far *fh, int forcemode);
int	FlashFreeFileX(FileHeader far *fh);	// for mserv
int	FlashCopy(unsigned char huge *dest, unsigned char huge *src, long count);
unsigned int	FlashCsum(void far *data, long size);

// this structure for NVRAM calling... doesn't really belong here..

typedef	struct	_fhelper {
	char	far	*filename;
	int		inlen;
	int		outlen;
	unsigned char	far	*source;
	unsigned char	far	*destination;
	SCD	far	*scd;
	int		mode;
	int		ucfilesize;
	FileHeader far	*fh;
	ControlBlock far *cb;
	long		date;
} FlashHelper;


#endif
