/******************************************************************************
 *                                                                            *
 * Copyright 1997 Advanced Micro Devices, Inc.                                *
 *                                                                            *
 * This software is the property of Advanced Micro Devices, Inc  (AMD)  which *
 * specifically  grants the user the right to modify, use and distribute this *
 * software provided this notice is not removed or altered.  All other rights *
 * are reserved by AMD.                                                       *
 *                                                                            *
 * AMD MAKES NO WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, WITH REGARD TO THIS *
 * SOFTWARE.  IN NO EVENT SHALL AMD BE LIABLE FOR INCIDENTAL OR CONSEQUENTIAL *
 * DAMAGES IN CONNECTION WITH OR ARISING FROM THE FURNISHING, PERFORMANCE, OR *
 * USE OF THIS SOFTWARE.                                                      *
 *                                                                            *
 * So that all may benefit from your experience, please report  any  problems *
 * or suggestions about this software back to AMD.  Please include your name, *
 * company,  telephone number,  AMD product requiring support and question or *
 * problem encountered.                                                       *
 *                                                                            *
 * Advanced Micro Devices, Inc.       Worldwide support and contact           *
 * Logic Products Division            information available at:               *
 * Systems Engineering                                                        *
 * 5204 E. Ben White Blvd.     http://www.amd.com/html/support/techsup.html   *
 * Austin, TX 78741                                                           *
 *****************************************************************************/

/* Various structures for use in PCI/PCNet */

#ifndef _PCN_H_
#define _PCN_H_

/* Configuration registers */

typedef volatile unsigned int VU16;
typedef unsigned int U16;
typedef volatile unsigned char VU8;
typedef unsigned char U8;


typedef  struct PcnInitBlock_s {
	VU16	tmode;		/* Mode, tlen & rlen */
	VU8	padr[6];	/* Physical address  */
	VU8	ladrf1[4];	/* Logical address filters */
	VU8	ladrf2[4];
	VU16	rdral;		/* Receive descriptor register address low order 16 */
	VU8	rdrah;		/* Receive descriptor register address high order 8 */
	VU8	rdrlen;		/* Receive descriptor register address high order 8 */
	VU16	tdral;		/* Transmit descriptor register address low order 16 */
	VU8	tdrah;		/* Transmit descriptor register address high order 8 */
	VU8	tdrlen;		/* Transmit descriptor register address high order 8 */
} PcnInitBlock_s;

/* Receive Descriptors */

typedef struct PcnRxDesc_s {
	VU16	rbadr;		/* Receive buffer address low order 15 bits*/
	VU16	rmd1;		/* Flags, high order 8 bits of buff address */
	VU16	rmd2;		/* Buffer byte count */
	VU16	rmd3;		/* Message byte count */
} PcnRxDesc_s;


#define RMD1_OWN	(1 << 15)	/* T = owned by controller */
#define RMD1_ERR	(1 << 14)	/* T = some type of error */
#define RMD1_FRAM	(1 << 13)	/* T = framing error */
#define RMD1_OFLO	(1 << 12)	/* T = overflow error */
#define RMD1_CRC	(1 << 11)	/* T = crc error */
#define RMD1_BUFF	(1 << 10)	/* T = buffer error */
#define RMD1_STP	(1 << 9)	/* T = start of packet */
#define RMD1_ENP	(1 << 8)	/* T = end of packet */
#define RMD1_ADDRH	(0xff)		/* high order address bits */

#define RMD2_ONES	(0xf << 12)	/* Must be 0's - set by host */
#define RMD_CNT	(0xfff)		/* # of bytes in received message */


typedef struct PcnTxDesc_s {
	VU16	tbadr;		/* Transmit buffer address low order 15 bits*/
	VU16	tmd1;		/* Flags, high order 8 bits of buff address */
	VU16	tmd2;		/* Buffer byte count */
	VU16	tmd3;		/* Various  transmit errors */
} PcnTxDesc_s;

#define TMD1_OWN	(1 << 15)	/* T = owned by controller */
#define TMD1_ERR	(1 << 14)	/* T = some type of error */
#define TMD1_ADDFCS	(1 << 13)	/* T = Add FCS to this frame */
#define TMD1_MORE	(1 << 12)	/* T = more than one retry */
#define TMD1_ONE	(1 << 11)	/* T = required one retry */
#define TMD1_DEF	(1 << 10)	/* T = had to defer */
#define TMD1_STP	(1 << 9)	/* T = start of packet */
#define TMD1_ENP	(1 << 8)	/* T = end of packet */
#define TMD1_ADDRH	(0xff)		/* high order address bits */

#define TMD2_ONES	(0xf << 12)	/* All ones - set by host */
#define TMD2_BCNT	(0xfff)		/* Buffer byte count - 2's complement */

#define TMD3_BUFF	(1 << 15)	/* T = buffer error */
#define TMD3_UFLO	(1 << 14)	/* T = underflow error */
#define TMD3_LCOL	(1 << 12)	/* T = Late collision */
#define TMD3_LCAR	(1 << 11)	/* T = Loss of carrier */
#define TMD3_RTRY	(1 << 10)	/* T = retry failed */

/* adressable PCnet registers */

#define RDP 		0x10	/* command & status register */
#define RAP 		0x12	/* register address port */
#define RESET 		0x14	/* reset register, read only */
#define IDP 		0x16	/* reset register, read only */

#define SELCSR0		0

/* Bit values for CSR registers */

#define CSR0_ERR	(1 << 15)
#define CSR0_BABL	(1 << 14)
#define CSR0_CERR	(1 << 13)
#define CSR0_MISS	(1 << 12)
#define CSR0_MERR	(1 << 11)
#define CSR0_RINT	(1 << 10)
#define CSR0_TINT	(1 << 9)
#define CSR0_IDON	(1 << 8)
#define CSR0_INTR	(1 << 7)
#define CSR0_INEA	(1 << 6)
#define CSR0_RXON	(1 << 5)
#define CSR0_TXON	(1 << 4)
#define CSR0_TDMD	(1 << 3)
#define CSR0_STOP	(1 << 2)
#define CSR0_STRT	(1 << 1)
#define CSR0_INIT	(1 << 0)


#endif //_PCN_H_

